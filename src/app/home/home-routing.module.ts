import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home.component';
import { PaymentHeaderComponent } from './payment-header/payment-header.component';


const homeRoutes: Routes = [
    {
        path: '',
        component: HomeComponent
        ,
        children: [
            {
                path: 'payment',
                component: PaymentHeaderComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(homeRoutes)],
    exports: [RouterModule]
})
export class HomeRoutingModule { }
