import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HomeComponent } from './home.component';
import { PaymentHeaderComponent } from './payment-header/payment-header.component';
import { HomeRoutingModule } from './home-routing.module';


@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        HomeRoutingModule
    ],
    declarations: [
        HomeComponent,
        PaymentHeaderComponent
    ],
    providers: []
})
export class HomeModule { }
